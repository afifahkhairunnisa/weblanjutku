<?php
class Manusia
{
	//menentukan property dengan private
	private $nama = 'malas ngoding';

	//method public 
	public function tampilkan_nama(){
		return "Nama saya" .$this->nama;
	}
}
$manusia = new Manusia ();
echo $manusia->tampilkan_nama() ."<br />";
echo $manusia->nama; //error karena aksesnya private